/**
 * This class contains test cases for booking of containers
 * @author Rohit Vadje
 */

package com.javatpoint.tests.booking;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.maersk.beans.BookingRequestBean;
import com.maersk.enums.ContainerTypeEnum;
import com.maersk.services.IContainerService;

class BookingTest {	

	@Autowired
	IContainerService containerService;

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	
	/**
	 * Booking Input With Wrong Capacity
	 */
	@Test
	public void bookingInputWithWrongCapacity() {	
		BookingRequestBean bookingRequestBean = new BookingRequestBean(0, ContainerTypeEnum.DRY, "India", "India", 0, new Timestamp(0));
		containerService.book(bookingRequestBean);		
		assertEquals(containerService.book(bookingRequestBean), 0);
	}
	
	/**
	 * Booking With Wrong Origin
	 */
	@Test
	public void bookingWithWrongOrigin() {	
		BookingRequestBean bookingRequestBean = new BookingRequestBean(20, ContainerTypeEnum.DRY, "India", "India", 0, new Timestamp(0));
		containerService.book(bookingRequestBean);		
		assertEquals(containerService.book(bookingRequestBean), 0);
	}
	
	/**
	 * Booking With Wrong Origin Extra Length
	 */
	@Test
	public void bookingWithWrongOriginExtraLength() {	
		BookingRequestBean bookingRequestBean = new BookingRequestBean(20, ContainerTypeEnum.DRY, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "India", 0, new Timestamp(0));
		containerService.book(bookingRequestBean);		
		assertEquals(containerService.book(bookingRequestBean), 0);
	}
	
	/**
	 * Booking With Wrong Origin Small Length
	 */
	@Test
	public void bookingWithWrongOriginSmallLength() {	
		BookingRequestBean bookingRequestBean = new BookingRequestBean(20, ContainerTypeEnum.DRY, "NN", "India", 0, new Timestamp(0));
		containerService.book(bookingRequestBean);		
		assertEquals(containerService.book(bookingRequestBean), 0);
	}
	
	/**
	 * Booking With Wrong Origin Special Chars
	 */
	@Test
	public void bookingWithWrongOriginSpecialChars() {	
		BookingRequestBean bookingRequestBean = new BookingRequestBean(20, ContainerTypeEnum.DRY, "~~~~~", "India", 0, new Timestamp(0));
		containerService.book(bookingRequestBean);		
		assertEquals(containerService.book(bookingRequestBean), 0);
	}
	
	
	
}
