/**
 * This class contains test cases for availability of containers
 * @author Rohit Vadje
 */

package com.javatpoint.tests.booking;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.maersk.beans.ExtAvailabilityRequestBean;
import com.maersk.enums.ContainerTypeEnum;
import com.maersk.services.IContainerService;

class AvailabilityTest {
	
	@Autowired
	IContainerService containerService;

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	/**
	 * Check Avialability With Wrong Container Size
	 */
	@Test
	public void checkAvialabilityWithWrongContainerSize() {
		ExtAvailabilityRequestBean ebb = new ExtAvailabilityRequestBean(90, ContainerTypeEnum.DRY, "ABCDE", "ABCDE", 0);
		assertEquals(containerService.checkAvialability(ebb), false);
	}
	
	/**
	 * Check Avialability With Wrong Origin less length
	 */
	@Test
	public void checkAvialabilityWithWrongOriginMinLength() {
		ExtAvailabilityRequestBean ebb = new ExtAvailabilityRequestBean(20, ContainerTypeEnum.DRY, "@@@", "ABCDE", 0);
		assertEquals(containerService.checkAvialability(ebb), false);
	}
	
	/**
	 * Check Avialability With Wrong Origin More length
	 */
	@Test
	public void checkAvialabilityWithWrongOriginMoreLength() {
		ExtAvailabilityRequestBean ebb = new ExtAvailabilityRequestBean(20, ContainerTypeEnum.DRY, "ABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", "ABCDE", 0);
		assertEquals(containerService.checkAvialability(ebb), false);
	}

}
