/**
 * MAERSK Container Types
 * @author Rohit Vadje
 */
package com.maersk.enums;

public enum ContainerTypeEnum {
	DRY, REEFER
}
