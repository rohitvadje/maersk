package com.maersk.beans;

import org.springframework.stereotype.Component;

@Component
public class BookingResponseBean {
	
	private long bookingRef;

	public long getBookingRef() {
		return bookingRef;
	}

	public void setBookingRef(long bookingRef) {
		this.bookingRef = bookingRef;
	}
	
	public BookingResponseBean() {
		// TODO Auto-generated constructor stub
	}

	public BookingResponseBean(long bookingRef) {
		super();
		this.bookingRef = bookingRef;
	}

	@Override
	public String toString() {
		return "BookingResponseBean [bookingRef=" + bookingRef + "]";
	}
	
}
