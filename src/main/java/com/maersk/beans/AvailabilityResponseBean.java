package com.maersk.beans;

import org.springframework.stereotype.Component;

@Component
public class AvailabilityResponseBean {

	private boolean available;

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public AvailabilityResponseBean() {
	}
	
	public AvailabilityResponseBean(boolean available) {
		super();
		this.available = available;
	}

	@Override
	public String toString() {
		return "AvailabilityResponseBean [available=" + available + "]";
	}

}
