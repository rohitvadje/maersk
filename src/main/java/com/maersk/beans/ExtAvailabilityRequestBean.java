package com.maersk.beans;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.maersk.enums.ContainerTypeEnum;

public class ExtAvailabilityRequestBean {
	
	@Positive
	private int containerSize;
	
	@NotEmpty
	@NotNull
	private ContainerTypeEnum containerType;
	
	@Size(min = 5, max = 20)
	private String origin;
	
	@Size(min = 5, max = 20)
	private String destination;
	
	@Range(min = 1, max = 100)
	private int quantity;
	
	public int getContainerSize() {
		return containerSize;
	}

	public void setContainerSize(int containerSize) {
		this.containerSize = containerSize;
	}

	public ContainerTypeEnum getContainerType() {
		return containerType;
	}

	public void setContainerType(ContainerTypeEnum containerType) {
		this.containerType = containerType;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public ExtAvailabilityRequestBean() {
	}

	public ExtAvailabilityRequestBean(int containerSize, ContainerTypeEnum containerType, String origin,
			String destination, int quantity) {
		super();
		this.containerSize = containerSize;
		this.containerType = containerType;
		this.origin = origin;
		this.destination = destination;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ExtAvailabilityRequestBean [containerSize=" + containerSize + ", containerType=" + containerType
				+ ", origin=" + origin + ", destination=" + destination + ", quantity=" + quantity + "]";
	}
	
}
