package com.maersk.beans;

import org.springframework.stereotype.Component;

@Component
public class ExtAvailabilityResponseBean {
	
	private int availableSpace;

	public int getAvailableSpace() {
		return availableSpace;
	}

	public void setAvailableSpace(int availableSpace) {
		this.availableSpace = availableSpace;
	}
	
	public ExtAvailabilityResponseBean() {
		// TODO Auto-generated constructor stub
	}

	public ExtAvailabilityResponseBean(int availableSpace) {
		super();
		this.availableSpace = availableSpace;
	}

	@Override
	public String toString() {
		return "AvialabilityResponseBean [availableSpace=" + availableSpace + "]";
	}
	
}
