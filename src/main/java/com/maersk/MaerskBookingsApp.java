package com.maersk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaerskBookingsApp {
	public static void main(String[] args) {
		SpringApplication.run(MaerskBookingsApp.class, args);
	}
}
