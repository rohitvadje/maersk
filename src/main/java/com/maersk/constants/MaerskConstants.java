package com.maersk.constants;

public enum MaerskConstants {
	
	CHECK_BOOKING_AVAILABILITY("https://maersk.com/api/bookings/checkAvailable"),
	CASSANDRA_KEYSPACE_NAME("maersk");
	
	private final String constant;
	
	MaerskConstants(String string) {
		constant = string;
	}
}
