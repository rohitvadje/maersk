package com.maersk.dao;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.maersk.beans.BookingRequestBean;

public interface IContainerDAO extends CassandraRepository<BookingRequestBean> {
	
	long book(BookingRequestBean bookingRequestBean);

}
