/**
 * This Repository contains CASSANDRA database methods to perform operations
 * @author Rohit Vadje
 */

package com.maersk.dao;

import java.util.Optional;

import org.springframework.data.cassandra.repository.MapId;
import org.springframework.stereotype.Repository;

import com.maersk.beans.BookingRequestBean;

@Repository
public class ContainerDAO implements IContainerDAO {

	@Override
	public <S extends BookingRequestBean> S save(S entity) {
		return null;
	}

	@Override
	public <S extends BookingRequestBean> Iterable<S> saveAll(Iterable<S> entities) {
		return null;
	}

	@Override
	public Optional<BookingRequestBean> findById(MapId id) {
		return null;
	}

	@Override
	public boolean existsById(MapId id) {
		return false;
	}

	@Override
	public Iterable<BookingRequestBean> findAll() {
		return null;
	}

	@Override
	public Iterable<BookingRequestBean> findAllById(Iterable<MapId> ids) {
		return null;
	}

	@Override
	public long count() {
		return 0;
	}

	@Override
	public void deleteById(MapId id) {
	}

	@Override
	public void delete(BookingRequestBean entity) {
	}

	@Override
	public void deleteAll(Iterable<? extends BookingRequestBean> entities) {
	}

	@Override
	public void deleteAll() {
	}

	@Override
	public long book(BookingRequestBean bookingRequestBean) {
		return 0;
	}

}
