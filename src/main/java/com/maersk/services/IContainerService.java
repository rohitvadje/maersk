package com.maersk.services;

import com.maersk.beans.AvailabilityResponseBean;
import com.maersk.beans.BookingRequestBean;
import com.maersk.beans.ExtAvailabilityRequestBean;

public interface IContainerService {

	AvailabilityResponseBean checkAvialability(ExtAvailabilityRequestBean containerRequestBean);

	long book(BookingRequestBean bookingRequestBean);
}
