/**
 * This service class contains methods to check availability and to book containers with MAERSK
 * @author Rohit Vadje
 */
package com.maersk.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.maersk.beans.AvailabilityResponseBean;
import com.maersk.beans.ExtAvailabilityResponseBean;
import com.maersk.beans.BookingRequestBean;
import com.maersk.beans.ExtAvailabilityRequestBean;
import com.maersk.constants.MaerskConstants;
import com.maersk.dao.IContainerDAO;

@Service
public class ContainerService implements IContainerService {

	/**
	 * Container DAO
	 */
	@Autowired
	private IContainerDAO containerDAO;

	/**
	 * To check Availability of Containers
	 */
	@Override
	public AvailabilityResponseBean checkAvialability(ExtAvailabilityRequestBean containerRequestBean) {

		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForObject(MaerskConstants.CHECK_BOOKING_AVAILABILITY.name(), String.class);
		Gson g = new Gson();
		ExtAvailabilityResponseBean s = g.fromJson(response, ExtAvailabilityResponseBean.class);
		AvailabilityResponseBean responseFromService = new AvailabilityResponseBean();
		if(containerRequestBean.getContainerSize()==20 || containerRequestBean.getContainerSize()==40) {
			if (s.getAvailableSpace() == 0) {
				responseFromService.setAvailable(false);
			} else {
				responseFromService.setAvailable(true);
			}
		} else {
			responseFromService.setAvailable(false);
		}
		return responseFromService;
	}

	/**
	 * To book containers
	 */
	@Override
	public long book(BookingRequestBean bookingRequestBean) {
		long result = 0;
		if(bookingRequestBean.getContainerSize()==20 || bookingRequestBean.getContainerSize()==40) {
			try {
				containerDAO.save(bookingRequestBean);
				long min = 957000001;
			    long max = 957999999;
				double random = Math.random() * (max - min + 1) + min;
				result = (long) random;
			} catch (Exception e) {
				e.printStackTrace();
				result = 0;
			}
		} else {
			result = 0;
		}
		return result;
	}
}
