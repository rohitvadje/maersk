/**
 * This controller contains web services to check availability of containers and book them with MAERSK
 * @author Rohit Vadje
 */
package com.maersk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maersk.beans.AvailabilityResponseBean;
import com.maersk.beans.BookingRequestBean;
import com.maersk.beans.BookingResponseBean;
import com.maersk.beans.ExtAvailabilityRequestBean;
import com.maersk.services.IContainerService;

@RequestMapping("/api/bookings/")
@RestController
public class MaerskBookingController {

	/**
	 * Container Service
	 */
	@Autowired
	private IContainerService containerService;
	
	/**
	 * To check Availability of containers
	 * @param containerRequestBean
	 * @return True if available otherwise false
	 */
	@PostMapping(value = "checkAvialability")
	public ResponseEntity<AvailabilityResponseBean> checkAvialability(
			@RequestBody ExtAvailabilityRequestBean containerRequestBean) {
		return new ResponseEntity<>(containerService.checkAvialability(containerRequestBean), HttpStatus.OK);
	}

	/**
	 * Booking container with given requirement
	 * @param bookingRequestBean
	 * @return Unique booking ID
	 */
	@PostMapping(value = "book")
	public ResponseEntity<BookingResponseBean> book(@RequestBody BookingRequestBean bookingRequestBean) {
		BookingResponseBean brb = new BookingResponseBean();
		long result = containerService.book(bookingRequestBean);
		if(result==0) {
			return new ResponseEntity<>(brb, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			brb.setBookingRef(result);
			return new ResponseEntity<>(brb, HttpStatus.CREATED);
		}	
	}

}
